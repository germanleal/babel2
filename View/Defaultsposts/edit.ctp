<div class="defaultsposts form">
<?php echo $this->Form->create('Defaultspost'); ?>
	<fieldset>
		<legend><?php echo __('Edit Defaultspost'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('templatename');
		echo $this->Form->input('title');
		echo $this->Form->input('body');
		echo $this->Form->input('building_id');
		echo $this->Form->input('group_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Defaultspost.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Defaultspost.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Defaultsposts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('controller' => 'buildings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('controller' => 'buildings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
