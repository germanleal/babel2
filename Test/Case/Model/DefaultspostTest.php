<?php
App::uses('Defaultspost', 'Model');

/**
 * Defaultspost Test Case
 *
 */
class DefaultspostTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.defaultspost',
		'app.building',
		'app.user',
		'app.group',
		'app.buildings_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Defaultspost = ClassRegistry::init('Defaultspost');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Defaultspost);

		parent::tearDown();
	}

}
