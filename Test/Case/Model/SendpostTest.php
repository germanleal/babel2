<?php
App::uses('Sendpost', 'Model');

/**
 * Sendpost Test Case
 *
 */
class SendpostTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sendpost',
		'app.user',
		'app.group',
		'app.post',
		'app.building',
		'app.buildings_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sendpost = ClassRegistry::init('Sendpost');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sendpost);

		parent::tearDown();
	}

}
