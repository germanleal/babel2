-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-03-2014 a las 02:41:48
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `babel2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `id` char(36) NOT NULL,
  `razonsocial` varchar(500) NOT NULL,
  `rut` varchar(20) NOT NULL,
  `numeropisos` int(11) NOT NULL,
  `departamentosporpiso` int(11) NOT NULL,
  `email` varchar(400) DEFAULT NULL,
  `presidentejuntavecinos` varchar(500) DEFAULT NULL,
  `emailjuntavecinos` varchar(400) DEFAULT NULL,
  `direccion` varchar(500) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `nombrebanco` varchar(400) NOT NULL,
  `tipocuentabancaria` varchar(100) NOT NULL,
  `numerocuentabancaria` varchar(100) NOT NULL,
  `rutcuentabancaria` varchar(20) NOT NULL,
  `transferirnombre` varchar(500) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rut` (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `buildings`
--

INSERT INTO `buildings` (`id`, `razonsocial`, `rut`, `numeropisos`, `departamentosporpiso`, `email`, `presidentejuntavecinos`, `emailjuntavecinos`, `direccion`, `telefono`, `nombrebanco`, `tipocuentabancaria`, `numerocuentabancaria`, `rutcuentabancaria`, `transferirnombre`, `created`, `modified`) VALUES
('53228c89-5a70-49b8-b670-0ebccacf5782', 'centro norte', '11111111-1', 22, 13, 'centronorte@localhost', '', '', 'san martin 870', '232323232', 'scotiabank', 'cuenta corriente', '123423423', '80111222-3', 'sociedad administradora centro norte', '2014-03-14 05:58:49', '2014-03-14 05:58:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buildings_users`
--

CREATE TABLE IF NOT EXISTS `buildings_users` (
  `building_id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `buildings_users`
--

INSERT INTO `buildings_users` (`building_id`, `user_id`, `active`, `created`, `modified`) VALUES
('53228c89-5a70-49b8-b670-0ebccacf5782', '53212e42-8b4c-40b1-ba11-0f2c687b5a76', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `defaultsposts`
--

CREATE TABLE IF NOT EXISTS `defaultsposts` (
  `id` char(36) NOT NULL,
  `templatename` varchar(600) NOT NULL,
  `title` varchar(600) NOT NULL,
  `body` text NOT NULL,
  `building_id` char(36) NOT NULL,
  `group_id` char(36) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `defaultsposts`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` char(36) NOT NULL,
  `building_id` char(36) NOT NULL,
  `numerodepartamento` varchar(20) NOT NULL,
  `user_id` char(36) NOT NULL,
  `numeropiso` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `departments`
--

INSERT INTO `departments` (`id`, `building_id`, `numerodepartamento`, `user_id`, `numeropiso`, `created`, `modified`) VALUES
('532291b5-e1dc-4be4-ba90-0ebccacf5782', '53228c89-5a70-49b8-b670-0ebccacf5782', '1510', '53212e42-8b4c-40b1-ba11-0f2c687b5a76', 15, '2014-03-14 06:20:53', '2014-03-14 06:20:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` char(36) NOT NULL,
  `groupname` varchar(600) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `groupname`, `created`, `modified`, `active`) VALUES
('53212d99-e3b4-44b0-bc88-0f2c687b5a76', 'SÃºper Admin', '2014-03-13 05:01:29', '2014-03-14 05:19:17', 1),
('53212dc2-7118-4075-93ba-0f2c687b5a76', 'Administrador Edificio', '2014-03-13 05:02:10', '2014-03-13 05:02:10', 1),
('53212dd1-8cb0-4695-b1e5-0f2c687b5a76', 'Conserje', '2014-03-13 05:02:25', '2014-03-13 05:02:25', 1),
('53212ddb-258c-434b-b8f9-0f2c687b5a76', 'Copropietario', '2014-03-13 05:02:35', '2014-03-13 05:02:35', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` char(36) NOT NULL,
  `title` varchar(600) NOT NULL,
  `body` text NOT NULL,
  `building_id` char(36) NOT NULL,
  `group_id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `posts`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sendposts`
--

CREATE TABLE IF NOT EXISTS `sendposts` (
  `id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `post_id` char(36) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `read` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `sendposts`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` char(36) NOT NULL,
  `name` varchar(600) NOT NULL,
  `building_id` char(36) NOT NULL,
  `description` varchar(600) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `name`, `building_id`, `description`, `created`, `modified`) VALUES
('53264a46-8468-4a42-83a6-1f74cacf5782', 'lavanderÃ­a', '53228c89-5a70-49b8-b670-0ebccacf5782', 'uso de lavanderÃ­a', '2014-03-17 02:05:10', '2014-03-17 02:05:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_users`
--

CREATE TABLE IF NOT EXISTS `services_users` (
  `id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `service_id` char(36) NOT NULL,
  `reservationdate` date NOT NULL,
  `reservationhour` time NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `services_users`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` char(36) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(400) DEFAULT NULL,
  `phonenumber` varchar(20) DEFAULT NULL,
  `group_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`, `firstname`, `lastname`, `email`, `phonenumber`, `group_id`) VALUES
('53212e42-8b4c-40b1-ba11-0f2c687b5a76', 'admin', 'fd8dd51ce30b8351fe0e78cf17abbc919ac9073a', '2014-03-13 05:04:18', '2014-03-14 05:05:11', 'root', 'local', 'root@localhost', '123456', '53212d99-e3b4-44b0-bc88-0f2c687b5a76');
