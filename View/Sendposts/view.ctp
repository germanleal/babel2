<div class="sendposts view">
<h2><?php  echo __('Sendpost'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sendpost['Sendpost']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($sendpost['User']['id'], array('controller' => 'users', 'action' => 'view', $sendpost['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Post'); ?></dt>
		<dd>
			<?php echo $this->Html->link($sendpost['Post']['title'], array('controller' => 'posts', 'action' => 'view', $sendpost['Post']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($sendpost['Sendpost']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($sendpost['Sendpost']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Read'); ?></dt>
		<dd>
			<?php echo h($sendpost['Sendpost']['read']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sendpost'), array('action' => 'edit', $sendpost['Sendpost']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sendpost'), array('action' => 'delete', $sendpost['Sendpost']['id']), null, __('Are you sure you want to delete # %s?', $sendpost['Sendpost']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sendposts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sendpost'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
	</ul>
</div>
