<div class="sendposts index">
	<h2><?php echo __('Sendposts'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('post_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('read'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($sendposts as $sendpost): ?>
	<tr>
		<td><?php echo h($sendpost['Sendpost']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($sendpost['User']['id'], array('controller' => 'users', 'action' => 'view', $sendpost['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($sendpost['Post']['title'], array('controller' => 'posts', 'action' => 'view', $sendpost['Post']['id'])); ?>
		</td>
		<td><?php echo h($sendpost['Sendpost']['created']); ?>&nbsp;</td>
		<td><?php echo h($sendpost['Sendpost']['modified']); ?>&nbsp;</td>
		<td><?php echo h($sendpost['Sendpost']['read']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $sendpost['Sendpost']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sendpost['Sendpost']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sendpost['Sendpost']['id']), null, __('Are you sure you want to delete # %s?', $sendpost['Sendpost']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sendpost'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
	</ul>
</div>
