<div class="buildings index">
	<h2><?php echo __('Buildings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('razonsocial'); ?></th>
			<th><?php echo $this->Paginator->sort('rut'); ?></th>
			<th><?php echo $this->Paginator->sort('numeropisos'); ?></th>
			<th><?php echo $this->Paginator->sort('departamentosporpiso'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('presidentejuntavecinos'); ?></th>
			<th><?php echo $this->Paginator->sort('emailjuntavecinos'); ?></th>
			<th><?php echo $this->Paginator->sort('direccion'); ?></th>
			<th><?php echo $this->Paginator->sort('telefono'); ?></th>
			<th><?php echo $this->Paginator->sort('nombrebanco'); ?></th>
			<th><?php echo $this->Paginator->sort('tipocuentabancaria'); ?></th>
			<th><?php echo $this->Paginator->sort('numerocuentabancaria'); ?></th>
			<th><?php echo $this->Paginator->sort('rutcuentabancaria'); ?></th>
			<th><?php echo $this->Paginator->sort('transferirnombre'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($buildings as $building): ?>
	<tr>
		<td><?php echo h($building['Building']['id']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['razonsocial']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['rut']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['numeropisos']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['departamentosporpiso']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['email']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['presidentejuntavecinos']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['emailjuntavecinos']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['direccion']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['telefono']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['nombrebanco']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['tipocuentabancaria']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['numerocuentabancaria']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['rutcuentabancaria']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['transferirnombre']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['created']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $building['Building']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $building['Building']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $building['Building']['id']), null, __('Are you sure you want to delete # %s?', $building['Building']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Building'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
