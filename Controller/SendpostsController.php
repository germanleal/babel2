<?php
App::uses('AppController', 'Controller');
/**
 * Sendposts Controller
 *
 * @property Sendpost $Sendpost
 */
class SendpostsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Sendpost->recursive = 0;
		$this->set('sendposts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Sendpost->exists($id)) {
			throw new NotFoundException(__('Invalid sendpost'));
		}
		$options = array('conditions' => array('Sendpost.' . $this->Sendpost->primaryKey => $id));
		$this->set('sendpost', $this->Sendpost->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Sendpost->create();
			if ($this->Sendpost->save($this->request->data)) {
				$this->Session->setFlash(__('The sendpost has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sendpost could not be saved. Please, try again.'));
			}
		}
		$users = $this->Sendpost->User->find('list');
		$posts = $this->Sendpost->Post->find('list');
		$this->set(compact('users', 'posts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Sendpost->exists($id)) {
			throw new NotFoundException(__('Invalid sendpost'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sendpost->save($this->request->data)) {
				$this->Session->setFlash(__('The sendpost has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sendpost could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Sendpost.' . $this->Sendpost->primaryKey => $id));
			$this->request->data = $this->Sendpost->find('first', $options);
		}
		$users = $this->Sendpost->User->find('list');
		$posts = $this->Sendpost->Post->find('list');
		$this->set(compact('users', 'posts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Sendpost->id = $id;
		if (!$this->Sendpost->exists()) {
			throw new NotFoundException(__('Invalid sendpost'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Sendpost->delete()) {
			$this->Session->setFlash(__('Sendpost deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Sendpost was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
