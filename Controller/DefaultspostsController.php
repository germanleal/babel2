<?php
App::uses('AppController', 'Controller');
/**
 * Defaultsposts Controller
 *
 * @property Defaultspost $Defaultspost
 */
class DefaultspostsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Defaultspost->recursive = 0;
		$this->set('defaultsposts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Defaultspost->exists($id)) {
			throw new NotFoundException(__('Invalid defaultspost'));
		}
		$options = array('conditions' => array('Defaultspost.' . $this->Defaultspost->primaryKey => $id));
		$this->set('defaultspost', $this->Defaultspost->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Defaultspost->create();
			if ($this->Defaultspost->save($this->request->data)) {
				$this->Session->setFlash(__('The defaultspost has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The defaultspost could not be saved. Please, try again.'));
			}
		}
		$buildings = $this->Defaultspost->Building->find('list', array(
				'fields' => array('Building.id','Building.razonsocial')
		));
		$groups = $this->Defaultspost->Group->find('list', array(
				'fields' => array('Group.id','Group.groupname')
		));
		$this->set(compact('buildings', 'groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Defaultspost->exists($id)) {
			throw new NotFoundException(__('Invalid defaultspost'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Defaultspost->save($this->request->data)) {
				$this->Session->setFlash(__('The defaultspost has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The defaultspost could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Defaultspost.' . $this->Defaultspost->primaryKey => $id));
			$this->request->data = $this->Defaultspost->find('first', $options);
		}
		$buildings = $this->Defaultspost->Building->find('list');
		$groups = $this->Defaultspost->Group->find('list');
		$this->set(compact('buildings', 'groups'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Defaultspost->id = $id;
		if (!$this->Defaultspost->exists()) {
			throw new NotFoundException(__('Invalid defaultspost'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Defaultspost->delete()) {
			$this->Session->setFlash(__('Defaultspost deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Defaultspost was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
