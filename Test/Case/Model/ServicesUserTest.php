<?php
App::uses('ServicesUser', 'Model');

/**
 * ServicesUser Test Case
 *
 */
class ServicesUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.services_user',
		'app.user',
		'app.group',
		'app.service',
		'app.building',
		'app.buildings_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ServicesUser = ClassRegistry::init('ServicesUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ServicesUser);

		parent::tearDown();
	}

}
