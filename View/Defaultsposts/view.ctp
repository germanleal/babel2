<div class="defaultsposts view">
<h2><?php  echo __('Defaultspost'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($defaultspost['Defaultspost']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Templatename'); ?></dt>
		<dd>
			<?php echo h($defaultspost['Defaultspost']['templatename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($defaultspost['Defaultspost']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($defaultspost['Defaultspost']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Building'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultspost['Building']['id'], array('controller' => 'buildings', 'action' => 'view', $defaultspost['Building']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultspost['Group']['id'], array('controller' => 'groups', 'action' => 'view', $defaultspost['Group']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($defaultspost['Defaultspost']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($defaultspost['Defaultspost']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Defaultspost'), array('action' => 'edit', $defaultspost['Defaultspost']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Defaultspost'), array('action' => 'delete', $defaultspost['Defaultspost']['id']), null, __('Are you sure you want to delete # %s?', $defaultspost['Defaultspost']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Defaultsposts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Defaultspost'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('controller' => 'buildings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('controller' => 'buildings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
