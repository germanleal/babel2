<div class="buildings view">
<h2><?php  echo __('Building'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($building['Building']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Razonsocial'); ?></dt>
		<dd>
			<?php echo h($building['Building']['razonsocial']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rut'); ?></dt>
		<dd>
			<?php echo h($building['Building']['rut']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Numeropisos'); ?></dt>
		<dd>
			<?php echo h($building['Building']['numeropisos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departamentosporpiso'); ?></dt>
		<dd>
			<?php echo h($building['Building']['departamentosporpiso']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($building['Building']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Presidentejuntavecinos'); ?></dt>
		<dd>
			<?php echo h($building['Building']['presidentejuntavecinos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Emailjuntavecinos'); ?></dt>
		<dd>
			<?php echo h($building['Building']['emailjuntavecinos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Direccion'); ?></dt>
		<dd>
			<?php echo h($building['Building']['direccion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telefono'); ?></dt>
		<dd>
			<?php echo h($building['Building']['telefono']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombrebanco'); ?></dt>
		<dd>
			<?php echo h($building['Building']['nombrebanco']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipocuentabancaria'); ?></dt>
		<dd>
			<?php echo h($building['Building']['tipocuentabancaria']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Numerocuentabancaria'); ?></dt>
		<dd>
			<?php echo h($building['Building']['numerocuentabancaria']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rutcuentabancaria'); ?></dt>
		<dd>
			<?php echo h($building['Building']['rutcuentabancaria']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Transferirnombre'); ?></dt>
		<dd>
			<?php echo h($building['Building']['transferirnombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($building['Building']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($building['Building']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Building'), array('action' => 'edit', $building['Building']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Building'), array('action' => 'delete', $building['Building']['id']), null, __('Are you sure you want to delete # %s?', $building['Building']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($building['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Firstname'); ?></th>
		<th><?php echo __('Lastname'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Phonenumber'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($building['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['created']; ?></td>
			<td><?php echo $user['modified']; ?></td>
			<td><?php echo $user['firstname']; ?></td>
			<td><?php echo $user['lastname']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['phonenumber']; ?></td>
			<td><?php echo $user['group_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
