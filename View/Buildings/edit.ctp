<div class="buildings form">
<?php echo $this->Form->create('Building'); ?>
	<fieldset>
		<legend><?php echo __('Edit Building'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('razonsocial');
		echo $this->Form->input('rut');
		echo $this->Form->input('numeropisos');
		echo $this->Form->input('departamentosporpiso');
		echo $this->Form->input('email');
		echo $this->Form->input('presidentejuntavecinos');
		echo $this->Form->input('emailjuntavecinos');
		echo $this->Form->input('direccion');
		echo $this->Form->input('telefono');
		echo $this->Form->input('nombrebanco');
		echo $this->Form->input('tipocuentabancaria');
		echo $this->Form->input('numerocuentabancaria');
		echo $this->Form->input('rutcuentabancaria');
		echo $this->Form->input('transferirnombre');
		echo $this->Form->input('User');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Building.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Building.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
