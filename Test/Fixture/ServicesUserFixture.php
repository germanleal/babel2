<?php
/**
 * ServicesUserFixture
 *
 */
class ServicesUserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'user_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'service_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'reservationdate' => array('type' => 'date', 'null' => false, 'default' => null),
		'reservationhour' => array('type' => 'time', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '53265245-23dc-46b7-b627-13cccacf5782',
			'user_id' => 'Lorem ipsum dolor sit amet',
			'service_id' => 'Lorem ipsum dolor sit amet',
			'reservationdate' => '2014-03-17',
			'reservationhour' => '02:39:17',
			'created' => '2014-03-17 02:39:17',
			'modified' => '2014-03-17 02:39:17'
		),
	);

}
