<?php
App::uses('BuildingsUser', 'Model');

/**
 * BuildingsUser Test Case
 *
 */
class BuildingsUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.buildings_user',
		'app.building',
		'app.user',
		'app.group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BuildingsUser = ClassRegistry::init('BuildingsUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BuildingsUser);

		parent::tearDown();
	}

}
